import unittest

zhtml = __import__('zhtml')


class TestInjection(unittest.TestCase):
    with open('examples/example-0.html', 'r') as templ_file:
        templ = zhtml.Page(templ_file.read(), 'example')
    
    with open('examples/example-1.html', 'r') as templ_file:
        templ_without_title = zhtml.Page(templ_file.read(), 'example')

    def test_title_inject(self):
        expected_element = r' {4}<title>example</title>'
        replaced_element = r'<title>zhtml Example</title>'

        templ_with_title = self.templ.inject_title()
        self.assertRegex(self.templ.text, replaced_element)
        self.assertRegex(templ_with_title.text, expected_element)
        self.assertNotRegex(templ_with_title.text, replaced_element)

        templ_with_title_2 = self.templ_without_title.inject_title()

        self.assertRegex(templ_with_title_2.text, expected_element)


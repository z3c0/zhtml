
import zhtml as html


if __name__ == '__main__':
    binding = html.Placeholder.create('APP')
    example = html.Page('examples/example.html', 'index')

    heading = html.heading('Hello, World!')
    paragraph = html.paragraph('This is a body of text.')

    main = html.main(heading + paragraph)

    example = example.inject_text(main, binding)
    example.write('output')

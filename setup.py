import setuptools
import configparser as cfg
import os

config = cfg.ConfigParser()
config.read('meta.ini')

path = os.path.dirname(os.path.abspath(__file__))

with open(path + '/README.md', 'r') as readme_file:
    long_description = str(readme_file.read())


NAME = 'zhtml'
DESCRIPTION = 'Because the world needs yet another HTML templating framework'
LICENSE = 'LGPL-3.0'
AUTHOR = 'z3c0'
AUTHOR_EMAIL = 'z3c0@21337.tech'
PYTHON_VERSION = '>=3.9'
GITHUB_URL = 'https://gitlab.com/z3c0/zhtml'

KEYWORDS = []
CLASSIFIERS = []

setup_kwargs = {'name': NAME,
                'author': AUTHOR,
                'author_email': AUTHOR_EMAIL,
                'packages': setuptools.find_packages(),
                'include_package_data': True,
                'version': config['default']['version'],
                'license': LICENSE,
                'description': DESCRIPTION,
                'long_description': long_description,
                'long_description_content_type': 'text/markdown',
                'url': GITHUB_URL,
                'keywords': KEYWORDS,
                'classifiers': CLASSIFIERS,
                'python_requires': PYTHON_VERSION}

setuptools.setup(**setup_kwargs)

# ZHTML (LGPLv3)

Because the world needs another HTML framework.

ZHTML is short for ZacHTML, which is short for Zach HTML.

ZHTML is intended for the server-side rendering of static web pages.

## Install

```bash
python -m pip install zhtml
```

## Tutorial

Create a blank HTML page to begin.

```html
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <title>zhtml Example</title>

    <!-- Water.css, to spruce things up -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">

</head>
<body>
    
</body>

</html>
```

ZHTML is intended for server-side rendering, so page elements are generated dynamically. As such, instead of putting our HTML directly into our file, we will instead insert a comment as a placeholder. This instructs the ZHTML engine where to insert the desired page elements.

```html
</head>

<body>
    <!-- APP -->
</body>

</html>
```

Next, let's begin our script.

```py
# example.py

import zhtml as html


if __name__ == '__main__':
    with open('examples/example.html', 'r') as example_file:
        example = html.Page(example_file.read(), 'index')

```

`html.Page` allows us to load our template file as an object.

Before going any further, we need to bind our `APP` placeholder, using the `html.Placeholder` class.

```py
# example.py

import zhtml as html


if __name__ == '__main__':
    binding = html.Placeholder('APP')
    with open('examples/example.html', 'r') as example_file:
        example = html.Page(example_file.read(), 'index')

```

By passing `'APP'` to our `Placeholder` object, we are telling it to look for the `<!-- APP -->` placeholder in our HTML file. The resulting object is an `re.Pattern`.

Next, let's create some HTML to inject into our `example.html` page.

ZHTML uses string functions to assemble HTML snippets. To start, let's create a heading and a paragraph.

```py
# example.py

import zhtml as html


if __name__ == '__main__':
    binding = html.Placeholder.create('APP')
    with open('examples/example.html', 'r') as example_file:
        example = html.Page(example_file.read(), 'index')

    heading = html.heading('Hello, World!')
    paragraph = html.paragraph('This is a body of text.')

```

Once created, lets wrap our new elements in a `main` tag. This will be the root of our application. Because ZHTML assembles elements as strings, unioning our heading and paragraph is as easy as concatenating two strings.

```py
# example.py

import zhtml as html


if __name__ == '__main__':
    binding = html.Placeholder.create('APP')
    with open('examples/example.html', 'r') as example_file:
        example = html.Page(example_file.read(), 'index')

    heading = html.heading('Hello, World!')
    paragraph = html.paragraph('This is a body of text.')

    main = html.main(heading + paragraph)

```

Finally, we can inject our application using the `inject_text` method on our `Page` object. Be sure to provide the `Placeholder` that we created earlier. To output the results, use the `write` method.

```py
# example.py

import zhtml as html


if __name__ == '__main__':
    binding = html.Placeholder.create('APP')
    with open('examples/example.html', 'r') as example_file:
        example = html.Page(example_file.read(), 'index')

    heading = html.heading('Hello, World!')
    paragraph = html.paragraph('This is a body of text.')

    main = html.main(heading + paragraph)

    example = example.inject_text(main, binding)
    example.write('output')

```

You should now have a file named `index.html` in the `output` folder of your project.

```html
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <title>zhtml Example</title>

    <!-- Water.css, to spruce things up -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">

</head>

<body>
    
    <main>
        <h1>Hello, World!</h1>
        <p>This is a body of text.</p>
    </main>
    
</body>

</html>
```

![index.html](examples/example.png)

***

## Contributing

ZHTML is a young project, with a lot of work to do. If you want to help [contact z3c0](mailto:z3c0@21337.tech), or fork the project and submit your change as a merge request.

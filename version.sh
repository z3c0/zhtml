#!/bin/bash

# read the version number from the meta.ini
version=$(awk -F "=" '/version/ {print $2}' meta.ini)

# split the version into the version and the prerelease version
IFS='-' read -ra versionArr <<< $version

versionNumber=${versionArr[0]:1}
prerelease=${versionArr[1]}

# split the version number into sections by period
IFS='.' read -ra versionNumberArr <<< $versionNumber

majorVersion=${versionNumberArr[0]}
minorVersion=${versionNumberArr[1]}
buildVersion=${versionNumberArr[2]}

if [[ -n $prerelease ]] && [ $buildVersion -gt 0 ]
then 
    newVersion="v$majorVersion.$minorVersion.$((buildVersion + 1))-$prerelease"
elif [[ -n $prerelease ]]
then
    newVersion="v$majorVersion.$minorVersion.$((buildVersion))-$prerelease"
else 
    newVersion="v$majorVersion.$minorVersion.$((buildVersion + 1))"
fi

matchingBranchCount=$(git -C ~/web branch --list | grep dev/$newVersion --count)

if [[ $matchingBranchCount -eq 0 ]]
then
    git -C ~/zhtml checkout master
    git -C ~/zhtml pull
    git -C ~/zhtml branch dev/$newVersion master
    git -C ~/zhtml checkout dev/$newVersion
    sed -i "s/\(version=\).*/\1$newVersion/" ~/zhtml/meta.ini
    git -C ~/zhtml add ~/zhtml/meta.ini
    git -C ~/zhtml commit -m "init dev/$newVersion"
    git -C ~/zhtml push -u origin dev/$newVersion
else
    echo "branch for $newVersion already exists"
fi
